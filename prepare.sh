#!/usr/bin/env bash

. .env > /dev/null 2> /dev/null


init() {
  if [ -z "$EJBCA_RELEASE" ]; then
    cat .versions >> .env
  else
    . ./.versions
    sed -i "s/WILDFLY_VERSION=.*/WILDFLY_VERSION=${WILDFLY_VERSION}/g" .env
    sed -i "s/EJBCA_MAJOR_VERSION.*/EJBCA_MAJOR_VERSION=${EJBCA_MAJOR_VERSION}/g" .env
    sed -i "s/EJBCA_RELEASE.*/EJBCA_RELEASE=${EJBCA_RELEASE}/g" .env
  fi
}

get_ejbca () {
  EJBCA_MINOR_VERSION="ejbca_${EJBCA_RELEASE}"

  EJBCA_DOWNLOAD_URL="https://sourceforge.net/projects/ejbca/files/${EJBCA_MAJOR_VERSION}/ejbca_ce_${EJBCA_RELEASE}.zip/download"
  EJBCA_DOWNLOAD_SHA256_URL="https://sourceforge.net/projects/ejbca/files/${EJBCA_MAJOR_VERSION}/ejbca_ce_${EJBCA_RELEASE}.zip.SHA-256/download"
  if [ ! -f "ejbca_ce_${EJBCA_RELEASE}.zip" ]; then
    curl -v -o "ejbca_ce_${EJBCA_RELEASE}.zip" -L "${EJBCA_DOWNLOAD_URL}"
    curl -o ejbca_ce_${EJBCA_RELEASE}.zip.SHA256 -L "${EJBCA_DOWNLOAD_SHA256_URL}"
    sha256sum --check ejbca_ce_${EJBCA_RELEASE}.zip.SHA256
    if [ $? -ne 0 ]; then
      echo "SHA256 for EJBCA does not match"
      rm -rf "ejbca_ce_${EJBCA_RELEASE}.zip" "ejbca_ce_${EJBCA_RELEASE}.zip.SHA256"
      exit 1
    fi
  fi
}


get_wildfly () {
  WILDFLY_DIR="wildfly-${WILDFLY_VERSION}"
  WILDFLY_TAR="wildfly-${WILDFLY_VERSION}.tar.gz"
  WILDFLY_DOWNLOAD_URL="https://download.jboss.org/wildfly/${WILDFLY_VERSION}/${WILDFLY_TAR}"
  MAJ_VER=$(echo $WILDFLY_VERSION | awk -F. '{print $1}')
  if [ $MAJ_VER -gt 14 ]; then
    WILDFLY_TAR_SHA1=$(curl -L "https://download.jboss.org/wildfly/${WILDFLY_VERSION}/${WILDFLY_TAR}.sha1")
  fi
  if [ ! -f ${WILDFLY_TAR} ]; then
    curl -o "${WILDFLY_TAR}" -L ${WILDFLY_DOWNLOAD_URL}
    if [ ! -z "$WILDFLY_TAR_SHA1" ]; then
      echo ${WILDFLY_TAR_SHA1} ${WILDFLY_TAR} > "${WILDFLY_TAR}.sha1"
      sha1sum --check ${WILDFLY_TAR}.sha1
      if [ $? -ne 0 ]; then
         echo "SHA1 for wildfly does not match"
         rm -f "${WILDFLY_TAR}"
         exit 1
      fi
      rm -f "${WILDFLY_TAR}.sha1"
    fi
  fi
}


init
. .versions > /dev/null 2> /dev/null

DLOAD_DIR="Download"
if [ ! -d "$DLOAD_DIR" ]; then
  mkdir "$DLOAD_DIR"
fi
cd "$DLOAD_DIR" || exit 1

get_ejbca
get_wildfly

