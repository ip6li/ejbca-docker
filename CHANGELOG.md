# 2020-12-28

* Migration from CentOS8 to Debian buster (due to change of release policy of CentOS8)
* Redesign of container architecture
* and - of course - lateste EJBCA community version

# Known Issues

While generating keys in phase *ant runinstall* EJBCA build script tries to create p12 directory
in users home directory. Workaround is to create a soft link to ~/ejbca/p12, otherwise
*ant runinstall* fails.

