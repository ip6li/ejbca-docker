from debian:buster-slim
maintainer Christian Felsing <support@felsing.net>

user root

ARG httpsserver_hostname
ARG database_host
ARG database_name
ARG database_username
ARG database_password
ARG database_type
ARG database_port
ARG superadmin_cn
ARG ca_name
ARG BASE_DN
ARG EJBCA_USER
ARG EJBCA_GRP
ARG EJBCA_HOME
ARG SMTP_FROM
ARG SMTP_HOST
ARG SMTP_PORT
ARG SMTP_TLS
ARG SMTP_USER
ARG SMTP_PASS
ARG WILDFLY_VERSION
ARG EJBCA_RELEASE
ARG keystorepass
ARG truststorepass
ARG httpsserver_password
ARG cmskeystorepass

run DEBIAN_FRONTEND=noninteractive apt-get update
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y apt-utils
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  locales

RUN sed -i -e 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG de_DE.UTF-8
ENV LANGUAGE de_DE:de
ENV LC_ALL de_DE.UTF-8

RUN DEBIAN_FRONTEND=noninteractive apt update && apt -q -y full-upgrade

RUN echo "postfix postfix/main_mailer_type string Internet site" > preseed.txt
RUN echo "postfix postfix/mailname string ${fqname}" >> preseed.txt
RUN debconf-set-selections preseed.txt

RUN \
  mkdir -p /usr/share/man/man1 \
  && DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  postfix \
  s-nail \
  procps \
  curl \
  unzip \
  bc

RUN if [ $(echo $EJBCA_RELEASE | awk -F_ '{print $1}') -le 6 ]; then \
    echo 'deb http://deb.debian.org/debian stretch main' > /etc/apt/sources.list.d/openjdk8-stretch.list ; \
    echo 'deb http://security.debian.org/debian-security stretch/updates main' >> /etc/apt/sources.list.d/openjdk8-stretch.list ; \
    printf "Package: *\nPin: release a=stable\nPin-Priority: 900\n" > /etc/apt/preferences.d/stable.pref ; \
    printf "Package: *\nPin: release a=oldstable\nPin-Priority: 50\n" > /etc/apt/preferences.d/oldstable.pref ; \
    DEBIAN_FRONTEND=noninteractive apt update ; \
    DEBIAN_FRONTEND=noninteractive apt-get install -q -y openjdk-8-jdk-headless ; \
  fi; \
  if [ $(echo $EJBCA_RELEASE | awk -F_ '{print $1}') -ge 7 ]; then \
    DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
    openjdk-11-jdk-headless; \
  fi ;

RUN \
  DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  ant \
  ant-contrib

RUN if [ -z "$database_type" ]; then echo "database_type not set"; exit 1; fi
RUN \
  if [ "$database_type" = "postgres" ]; then \
    DEBIAN_FRONTEND=noninteractive apt-get install -q -y postgresql-client libpostgresql-jdbc-java ; \
  fi
RUN \
  if [ "$database_type" = "mysql" ]; then \
    DEBIAN_FRONTEND=noninteractive apt-get install -q -y mariadb-client libmariadb-java ; \
  fi

RUN \
  groupadd ejbca \
  && useradd -d /home/ejbca -g ejbca -s /bin/bash ejbca

ADD ./Download /tmp/Download

copy ./entrypoint.sh /root/entrypoint.sh
run chown root:root /root/entrypoint.sh \
  && chmod 700 /root/entrypoint.sh
entrypoint ["/root/entrypoint.sh"]

