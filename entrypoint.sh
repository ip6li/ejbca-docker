#!/bin/bash

cat > ${EJBCA_HOME}/entrypoint.ejbca.sh <<EOF
cd ~ejbca || exit 1

if [ -d ejbca-custom ]; then
  if [ -f ejbca-setup ]; then
    rm -f ejbca-setup
  fi

  . ./environment
  export JAVA_OPTS
  echo "Run EJBCA"
  wildfly/bin/standalone.sh -b 0.0.0.0
  echo "EJBCA has terminated"
  return 1
else
  echo "Install EJBCA"

  cp /tmp/Download/ejbca-setup ${EJBCA_HOME}/ejbca-setup
  ./ejbca-setup
  echo "ejbca-setup has finished"
  return 0
fi
EOF

if [ "$database_type" = "postgres" ]; then
  cp "/usr/share/java/postgresql.jar" "${EJBCA_HOME}/wildfly/standalone/deployments/db-java-client.jar" > /dev/null 2> /dev/null
fi
if [ "$database_type" = "mysql" ]; then
  cp "/usr/share/java/mariadb-java-client.jar" "${EJBCA_HOME}/wildfly/standalone/deployments/db-java-client.jar" > /dev/null 2> /dev/null
fi
chown -R ejbca:ejbca ${EJBCA_HOME}
chmod 700 ${EJBCA_HOME}/entrypoint.ejbca.sh
if [ -f ${EJBCA_HOME}/ejbca-setup ]; then
  chmod 700 ${EJBCA_HOME}/ejbca-setup
fi

/usr/sbin/postfix start

/usr/bin/setpriv --init-groups --reuid=ejbca --regid=ejbca ${EJBCA_HOME}/entrypoint.ejbca.sh

exit $?

