# Docker build EJBCA Server From Scratch

## Usage

Create initial volume: ./prepare.sh

Create an .env file, see .env.example

Select a combination of EJBCA and Wildfly from

```
.versions.ejbca6wildfly14
.versions.ejbca7wildfly14
.versions.ejbca7wildfly18
```

copy one of these files to *.version* and Start prepare script for downloading required files:

    prepare.sh

Please note that EJBCA 7.4.0 is not available for public download yet, please see
https://www.ejbca.org/news/pre-release-get-ejbca-community-7-4-0-before-its-official-release/
If you got download link from Primekey, please move zip file to Download folder.

**Warning:** EJBCA6 needs an outdated OpenJDK version, which is a security risk. Use this
only if you really know what you are doing.

Build container

    docker-compose build

# Preparing Database

Postgres and Mariadb are supported.

## Preparing Postgres

Set up Postgres with following SQL commands:

```
CREATE DATABASE ejbca7;
create user ejbca7 with encrypted password 'A*Very*Bad*Database*Password';
grant all privileges on database ejbca7 to ejbca7;
```

## Preparing Mariadb

**Warning:** EJBCA6 cannot be installed on MariaDB due to outdated Hibernation settings in source. Using
an existing MariaDB EJBCA instance is possible.

Set up Mariadb with following SQL commands:

```
create database ejbca7 default character set = utf8 default collate = utf8_general_ci;
GRANT ALL PRIVILEGES ON ejbca7.* TO 'ejbca7'@'%' IDENTIFIED BY 'A*Very*Bad*Database*Password';
```

# Presets

You may set up some presets for EJBCA configuration.

## Preset passwords (optional), if not set random passwords are generated

Password presets may be handy, if you need to migrate from an existing EJBCA configuration.

```
keystorepass=*A*Very*Bad*Password*
truststorepass=*A*Very*Bad*Password*
httpsserver_password=*A*Very*Bad*Password*
cmskeystorepass=*A*Very*Bad*Password*
```

## Bind IP

Due to security reasons you should bind EJBCA ports to specific, internal IP adresses of
your Docker host.

BIND_AJP=192.168.42.1:8009
BIND_HTTP=192.168.42.1:8080
BIND_HTTPS=192.168.42.1:8442
BIND_ADMIN=192.168.42.1:8443

# Install EJBCA Container

You need to customize docker-compose.yml, check *ports:* and other network related settings.
After that you can start installation process with:

    docker-compose up

First start builds EJBCA and terminates then. While installation docker-compose ps may
show *unhealty*, but in this phase you do not need to care about this. If install
phase has finished with *success* then state must change to **Up (healthy)** otherwise
your EJBCA installation will probably not work.

# Let's Go

After installation docker-compose restarts ejbca container, because it terminates after
installation. After a few minutes **docker-compose ps** should show following:

```
        Name                 Command            State                                        Ports                                  
----------------------------------------------------------------------------------------------------------------------
ejbca-docker_ejbca_1   /root/entrypoint.sh   Up (healthy)   192.168.48.1:8009->8009/tcp, 192.168.48.1:8080->8080/tcp,
                                                            192.168.48.1:8442->8442/tcp, 192.168.48.1:8443->8443/tcp 
```

If you start ejbca container manually it fires up EJBCA instance directly without a
new installation.

    docker-compose up && docker-compose logs --tail=20 -ft

or use simply enclosed script

    restart

You may submit **-v** to restart, in this case logging is shown until ^C is pressed.

# Security Considerations

This builds an EJBCA as is. It is strongly recommended to use a reverse Proxy
server in front of EJBCA to filter access.
Wildfly may open some ports which should not be exposed to Internet - shit may happen.

# Content

Container is built an Debian buster, Wildfly 14.0.1 and EJBCA 7.4.0 Community (prerelease)
Wildfly 14.0.1 is not recommended by Primekey yer, but it seems working. If you aer fearless,
you man set Wildfly version to 18.0.1 in file **.versions**.
If you changed .versions you must start prepare.sh.

# EJBCA cli

Use script **ejbca.sh** for access to ejbca cli.

# GUI

After successful installation you should be able to connect with

    https://192.168.48.1:8443/ejbca/adminweb/

and you installed client certificate superadmin.p12 located at ./ejbca-home/ejbca/p12

